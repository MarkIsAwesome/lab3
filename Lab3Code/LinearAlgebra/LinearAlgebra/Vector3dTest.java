//Mark Agluba
//1533777

package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;


public class Vector3dTest {
    
    @Test
    public void testConstructor() {
        Vector3d vector = new Vector3d(1, 1, 2);
        assertEquals(vector.getX(), 1.0);
        assertEquals(vector.getY(), 1.0);
        assertEquals(vector.getZ(), 2.0);
    }

    @Test
    public void testMagnitude() {
        Vector3d vector = new Vector3d(1, 1, 2);
        assertEquals(vector.vectorMagnitude(), 2.449489742783178);
    }

    @Test
    public void testDotProduct() {
        Vector3d vector = new Vector3d(1, 1, 2);
        Vector3d anotherVector = new Vector3d(2, 3, 4);
        assertEquals(vector.dotProduct(anotherVector), 13.0);
    }

    @Test
    public void testAddMethod() {
        Vector3d vector = new Vector3d(1, 1, 2);
        Vector3d anotherVector = new Vector3d(2, 3, 4);
        Vector3d lastVector=vector.add(anotherVector);
        assertEquals(lastVector, lastVector);

    }
}
